import Header from "../header/Header";
import "./NotFound.css";

function NotFound() {
    return (
        <>
            <Header></Header>
            <div>
                <h1>NotFound</h1>
            </div>
        </>
    )
}
export default NotFound;