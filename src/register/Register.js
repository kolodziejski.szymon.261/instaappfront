import "./Register.css";
import {useCookies} from "react-cookie";
import {Link, useNavigate} from "react-router-dom";

function Register() {
    const [cookies, setCookie, removeCookie] = useCookies();
    const navigate = useNavigate();
    if (cookies.token) navigate('/');
    const sendForm = async (e) => {
        e.preventDefault();
        const user = {};
        const inputs = document.querySelectorAll('input');
        let valid = true;
        for (let input of inputs) {
            if (input.type === 'checkbox' && input.checked === false) return;
            if (input.type === 'submit' || input.type === 'checkbox') continue;
            if (input.value.length < 3) {
                setWrong(input);
                valid = false;
            }
            if (input.name === 'email' && !input.value.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)) {
                setWrong(input);
                valid = false;
            }
            user[input.name] = input.value;
        }


        if (!valid) return;
        const res = await fetch('http://localhost:3000/api/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user),
        });
        const data = await res.json();
        console.log(data);
        if (data.status === 201) {
            alert(data.message)
            navigate('/login');
        }
    }

    const setWrong = (i) => {
        i.style.outline = '1px solid red';
        setTimeout(() => {
            i.style.outline = 'none';
        }, 500);
    }
    return (
        <div className="FormContainer">
            <input type={'text'} placeholder={'Email'} name="email"></input>
            <input type={'password'} placeholder={'Hasło'} name="password"></input>
            <input type={'text'} placeholder={'Imię'} name="imie"></input>
            <input type={'text'} placeholder={'Nazwisko'} name="nazwisko"></input>
            <div id="zgodaDiv">
                <input type={'checkbox'} name='zgoda' id='zgoda'></input>
                <label htmlFor="zgoda">Zgadzam się blabla</label>
            </div>
            <input onClick={sendForm} type='submit' name="submit" value='Register' id='submit'></input>
            <div className="spacer"></div>
            <span>Masz już konto? <Link to="/login">Zaloguj się</Link></span>
        </div>
    )
}

export default Register;