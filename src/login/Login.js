import "./Login.css";
import "../variables.css";
import {useCookies} from "react-cookie";
import {Link, useNavigate} from "react-router-dom";

function Login() {
    const [cookies, setCookie, removeCookie] = useCookies();
    const navigate = useNavigate();
    if (cookies.token) navigate('/');
    const sendForm = async (e) => {
        e.preventDefault();
        const credentials = {};
        const inputs = document.querySelectorAll('input');
        for (let input of inputs) {
            if (input.type === 'submit') continue;
            if (input.value.length < 1) setWrong(input);
            credentials[input.name] = input.value;
        }
        const res = await fetch('http://localhost:3000/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials),
        });
        const data = await res.json();
        if (data.status === 400) {
            if (data.message.startsWith("Couldn't find user"))
                alert('Nie ma takiego użytkownika'); //TODO: zmienić alerty na te małe teksty na dole xd
            if (data.message.startsWith("Password incorrect"))
                alert('Złe hasło'); //TODO: zmienić alerty na te małe teksty na dole xd
        }
        if (data.status === 200) {
            let aaa = new Date();
            const time = aaa.getTime();
            const expireTime = time + 1000 * 60 * 30; //30 min
            aaa.setTime(expireTime);
            setCookie('token', data.token, {path: '/', expires: aaa});
            const res = await fetch('http://localhost:3000/api/profile', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${data.token}`,
                },
            });
            const data2 = await res.json();
            console.log(data2)
            setCookie('user', data2.profile, {path: '/', expires: aaa});

            navigate('/');
        }
    }

    const setWrong = (i) => {
        i.style.outline = '1px solid red';
        setTimeout(() => {
            i.style.outline = 'none';
        }, 500);
    }
    return (
        <div className="FormContainer">
            <input type={'text'} placeholder={'Email'} name="email"></input>
            <input type={'password'} placeholder={'Hasło'} name="password"></input>
            <input onClick={sendForm} type={'submit'} name="submit" value={'Login'} id='submit'></input>
            <div className="spacer"></div>
            <span>Nie masz konta? <Link to="/register">Zarejestruj się</Link></span>
        </div>
    )
}

export default Login;