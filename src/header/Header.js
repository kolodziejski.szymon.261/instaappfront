import "./Header.css";
import "../variables.css";
import {useCookies} from "react-cookie";
import {Link} from "react-router-dom";

function Header() {
    const [cookies, setCookie, removeCookie] = useCookies();

    function logout() {
        removeCookie('token');
        removeCookie('user');
        window.location = '/login';
    }

    return (<header>
        <Link to="/">Feed</Link>
        <Link to="/upload">Upload</Link>
        <Link to="/tags">Tags</Link>
        <div className="spacer"></div>
        {cookies.token ? <>
            <Link to={"/profile"}>Profil</Link>
            <Link to="#" onClick={logout}>Wyloguj</Link>
        </> : <>
            <Link to="/login">Zaloguj</Link>
            <Link to="/register">Zarejestruj</Link>
        </>}
    </header>)
}

export default Header;