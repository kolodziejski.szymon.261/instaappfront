import './variables.css';
import './App.css';
import React from "react";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Register from './register/Register';
import Login from './login/Login';
import NotFound from './NotFound/NotFound';
import Feed from "./LoggedIn/Feed/Feed";
import Upload from "./LoggedIn/Upload/Upload";
import PhotoInfo from "./LoggedIn/PhotoInfo/PhotoInfo";
import Profile from "./LoggedIn/Profile/Profile";
import Tags from "./LoggedIn/Tags/Tags";
import Filters from "./LoggedIn/PhotoInfo/Filters/Filters";

function App() {
    return (
        <div className="App">
            <Router>
                <Routes>
                    <Route exact path="/login" element={<Login/>}/>
                    <Route exact path="/register" element={<Register/>}/>
                    <Route exact path="/upload" element={<Upload/>}/>
                    <Route exact path="/profile" element={<Profile/>}/>
                    <Route exact path="/tags" element={<Tags/>}/>
                    <Route path="/photo/:id" element={<PhotoInfo/>}/>
                    <Route path="/filters/:id" element={<Filters/>}/>
                    <Route exact path="/" element={<Feed/>}/>
                    <Route path='/*' element={<NotFound/>}/>
                </Routes>
            </Router>
        </div>
    );
}

export default App;
