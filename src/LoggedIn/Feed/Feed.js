import "./Feed.css";
import "../../variables.css";
import {useEffect} from "react";
import {useCookies} from "react-cookie";
import AllPhotos from "../AllPhotos/AllPhotos";
import Header from "../../header/Header";
import {useNavigate} from "react-router-dom";


function Feed() {
    const [cookies, setCookie, removeCookie] = useCookies();
    const navigate = useNavigate();
    useEffect(() => {
        const token = cookies.token;
        console.log(token);
        if (!token) navigate('/login');
    }, []);

    return (
        <>
            <Header></Header>
            <AllPhotos></AllPhotos>
        </>
    )
}

export default Feed;