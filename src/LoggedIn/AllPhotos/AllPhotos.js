import "./AllPhotos.css";
import "../../variables.css";
import {useEffect, useState} from "react";
import axios from "axios";
import Photo from "../Photo/Photo";
import {useCookies} from "react-cookie";
import Masonry, {ResponsiveMasonry} from "react-responsive-masonry";


function AllPhotos() {
    const [cookies, setCookie, removeCookie] = useCookies();
    const [data, setData] = useState([]);
    useEffect(() => {
        fetchData();
    }, []);

    const getUser = async (id) => {
        let res;
        try {
            res= await axios
                .get(`http://localhost:3000/api/users/${id}`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${cookies.token}`,
                    }
                });
        } catch (e) {
            console.warn(e);
        }
        return res.data.user;
    }
    const fetchData = async () => {
        const res = await axios
            .get('http://localhost:3000/api/images/public', {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${cookies.token}`,
                }
            })
        const images = [];
        for (let img of res.data.images) {
            const user = await getUser(img.userId);
            images.push({
                img, user
            })
        }
        setData(images);
    };


    return (
        <ResponsiveMasonry columnsCountBreakPoints={{350: 1, 750: 2, 900: 3}}>
            <Masonry gutter="20px" style={{padding: "20px"}}>
                {data?.map((image) => <Photo key={image.img._id} user={image.user} img={image.img}></Photo>)}
            </Masonry>
        </ResponsiveMasonry>
    )
}

export default AllPhotos;