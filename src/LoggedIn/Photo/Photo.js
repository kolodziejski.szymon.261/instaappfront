import "./Photo.css";
import "../../variables.css";
import {useNavigate} from "react-router-dom";

const Photo = (props) => {
    const {img, user} = props;
    const navigate = useNavigate();
    const r = () => {
        navigate(`/photo/${img._id}`);
    }
    return (
        <div className="photo">
            <h1>{user.email}</h1>
            <img onClick={r} src={`http://localhost:3000/api/getfile/${img._id}`} alt="data.originalName"/>
            <div>
            <h3>{img.album + '/' + img.originalName}</h3>
            </div>
        </div>
    )
}

export default Photo;