import "./Tags.css";
import "../../variables.css";
import {useEffect} from "react";
import {useCookies} from "react-cookie";
import Header from "../../header/Header";
import {useNavigate} from "react-router-dom";


function Tags() {
    const [cookies, setCookie, removeCookie] = useCookies();
    const navigate = useNavigate();
    useEffect(() => {
        const token = cookies.token;
        console.log(token);
        if (!token) navigate('/login');
    }, []);

    const send = async () => {
        const tag = document.getElementById('tag');
        if (!tag.value || tag.value.length <= 3 || tag.value.split('')[0]!=='#') return setWrong(tag);

        const res = await fetch('http://localhost:3000/api/tags/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${cookies.token}`,
            },
            body: JSON.stringify({
                name: tag.value,
            }),
        });
        const data = await res.json();
        if (data.status === 400) {
            alert(data.message);
        }
        if (data.status === 200) {
            alert('Udało się utworzyć tag');
            tag.value = '';
        }
    }

    const setWrong = (i) => {
        i.style.outline = '1px solid red';
        setTimeout(() => {
            i.style.outline = 'none';
        }, 500);
    }

    return (
        <>
            <Header></Header>
            <main>
                <div className="test"></div>
                <div style={{marginTop: '3.5rem'}} className='uploadForm FormContainer'>
                    <input type="text" placeholder="tag" name="tag" id="tag"/>
                    <div className="spacer"></div>
                    <input type="submit" id='submit' className='button' onClick={send} value="submit"/>
                </div>
            </main>
        </>
    )
}

export default Tags;