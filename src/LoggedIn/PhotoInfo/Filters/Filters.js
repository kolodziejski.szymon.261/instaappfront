import "./Filters.css";
import "../../../variables.css";
import {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import axios from "axios";
import {useCookies} from "react-cookie";
import Header from "../../../header/Header";

const Filters = () => {
    const navigate = useNavigate();
    const {id} = useParams();
    const [cookies, setCookie, removeCookie] = useCookies();
    const [image, setImage] = useState({});

    const [currentFilter, setCurrentFilter] = useState({
        name: "rotate",
        options: {
            angle: 0
        }
    });
    useEffect(() => {
        const getImage = async () => {
            const res = await axios
                .get(`http://localhost:3000/api/images/${id}`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${cookies.token}`,
                    }
                });
            setImage(res.data.image);
        }
        getImage();
    }, [id]);


    const updateFilter = e => {
        console.log(e.target.value)
        switch (e.target.value) {
            case "rotate":
                reset();
                setCurrentFilter({
                    name: 'rotate',
                    options: {
                        angle: 0
                    }
                });
                break;
            case "grayscale":
                reset();
                document.getElementById('filterImage').style.setProperty('--grayscale', `1`);
                setCurrentFilter({
                    name: 'grayscale',
                    options: {}
                });
                break;
            case "blur":
                reset();
                setCurrentFilter({
                    name: 'blur',
                    options: {
                        px: 0
                    }
                });
                break;
        }
    }

    const reset = () => {
        document.getElementById('filterImage').style.setProperty('--rotate', `0deg`);
        document.getElementById('filterImage').style.setProperty('--blur', `0px`);
        document.getElementById('filterImage').style.setProperty('--grayscale', `0`);
    }

    const updateRotate = e => {
        setCurrentFilter({
            name: 'rotate',
            options: {
                angle: parseInt(e.target.value)
            }
        });
        document.getElementById('filterImage').style.setProperty('--rotate', `${e.target.value}deg`);
    }

    const updateBlur = e => {
        setCurrentFilter({
            name: 'blur',
            options: {
                px: parseInt(e.target.value)
            }
        });
        document.getElementById('filterImage').style.setProperty('--blur', `${e.target.value}px`);
        console.log(document.getElementById('filterImage').style.getPropertyValue('--blur'));
    }




    const sendUpdate = async () => {
        const res = await axios
            .patch(`http://localhost:3000/api/filters`, {
                _id: id,
                name: currentFilter.name,
                options: currentFilter.options
            }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${cookies.token}`,
                }
            });
        console.log(res);
        if (res.status === 200) {
            navigate(`/`);
        }
    }

    return (
        <>
            <Header></Header>
            <main>
                <div className="FormContainer" style={{marginTop: "2.5rem"}}>
                    <select name="filter" id="filterSelect" onChange={updateFilter} defaultValue={currentFilter.name}>
                        <option value="rotate">Rotate</option>
                        <option value="grayscale">grayscale</option>
                        <option value="blur">blur</option>
                    </select>

                    {currentFilter.name === "rotate" ?
                        <input onChange={updateRotate} type="range" name="angle" id="angle" defaultValue="0" max="360"
                               min="0" step="1"/> : <></>}
                    {currentFilter.name === "blur" ? <>
                        <input onChange={updateBlur} type="range" name="px" id="px" defaultValue="1" max="20" min="0"
                               step="1"/>
                    </> : <></>}

                    <div className="spacer"></div>
                    <input type="submit" id='submit' className='button' onClick={sendUpdate} value="submit"/>

                </div>
                <div className="FormContainer" style={{marginTop: "2.5rem"}}>
                    <div id="wrapper"><img id="filterImage" src={`http://localhost:3000/api/getfile/${image?._id}`}
                                           alt={image?.originalName || 'image'}/></div>
                </div>
            </main>
        </>
    )
}

export default Filters;