import "./PhotoInfo.css";
import "../../variables.css";
import {useNavigate, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";
import {useCookies} from "react-cookie";
import Header from "../../header/Header";
import Tag from "./Tag/Tag";

const PhotoInfo = () => {
    const navigate = useNavigate();
    const {id} = useParams();
    const [cookies, setCookie, removeCookie] = useCookies();
    const [image, setImage] = useState({});
    const [imageUser, setImageUser] = useState({});
    const [allTags, setAllTags] = useState([]);
    useEffect(() => {
        const token = cookies.token;
        console.log(token)
        if (!token) navigate('/login');
    }, []);
    useEffect(() => {
        getPhoto();
        getTags();
    }, []);

    useEffect(() => {
        console.log('image', image.userId);
        if (image.userId) getPhotoUser();
        console.log('imageUser', imageUser)
    }, [image]);
    const getTags = async () => {
        let res;
        try {
            res = await axios
                .get(`http://localhost:3000/api/tags/`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${cookies.token}`,
                    }
                });
        } catch (e) {
            console.warn(e);
        }
        setAllTags(res.data.tags);
    }
    const getPhoto = async () => {
        const res = await axios
            .get(`http://localhost:3000/api/images/${id}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${cookies.token}`,
                }
            });
        setImage(res.data.image);
    }

    const getPhotoUser = async () => {
        const res = await axios
            .get(`http://localhost:3000/api/users/${image.userId}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${cookies.token}`,
                }
            });
        setImageUser(res.data.user);
    }
    const updateTags = async () => {
        let tags = [];
        document.querySelectorAll('input[name="tag"]:checked').forEach(i => tags.push(i.value));
        const res = await axios
            .put(`http://localhost:3000/api/images/tags`, {
                _id: id,
                tags: tags,
            }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${cookies.token}`,
                }
            });
        if (res.status === 200) {
            getPhoto();
            setAllTags([]);
            getTags();
        }
    }

    const deleteImage = async () => {
        const res = await axios
            .delete(`http://localhost:3000/api/images/${id}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${cookies.token}`,
                }
            });
        if (res.status === 200) {
            navigate('/');
        }
        else {
            alert(res.data.message);
        }
    }

    return (
        <>
            <Header></Header>
            <main className='pin'>
                <div className="pi">
                    <div className="FormContainer photoInfo" style={{marginTop: "2.5rem"}}>
                        {imageUser && <h1>{imageUser.email}</h1>}
                        <img src={`http://localhost:3000/api/getfile/${image._id}`} alt="niewiem"/>
                        {image && <h1>{image.album} / {image.originalName}</h1>}
                        {cookies?.user?._id === image.userId ?
                            <><input type="submit" id='submit' className='button' onClick={() => {navigate(`/filters/${image._id}`)}} value="filtry"/>
                            <input type="submit" id='submit' className='button' onClick={deleteImage} value="delete"/> </>:
                            <></>}
                    </div>
                </div>
                <div className="FormContainer tags" style={{marginTop: "2.5rem"}}>
                    {image.tags && image.tags.map((tag, index) => <Tag key={tag} id={tag}/>)}
                </div>
                {cookies?.user?._id === image.userId ? <div className="FormContainer tags" style={{marginTop: "2.5rem"}}>
                    {allTags.map((tag, index) => {
                        return (
                            <>
                                <input defaultChecked={image.tags.find(i => i === tag._id)} type="checkbox" name="tag"
                                       id={tag._id} value={tag.name}/>
                                <label htmlFor={tag._id}><Tag key={tag._id} id={tag._id}/></label>
                            </>
                        )
                    })}
                    <div className="spacer"></div>
                    <input type="submit" id='submit' className='button' onClick={updateTags} value="submit"/>
                </div> : <></>}
            </main>
        </>
    )
}

export default PhotoInfo;