import "./Tag.css";
import "../../../variables.css";
import {useEffect, useState} from "react";
import axios from "axios";
import {useCookies} from "react-cookie";

const Tag = (props) => {
    const {id} = props;
    const [cookies, setCookie, removeCookie] = useCookies();
    const [tag, setTag] = useState({});
    useEffect(() => {
        const getTag = async () => {
            const res = await axios
                .get(`http://localhost:3000/api/tags/${id}`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${cookies.token}`,
                    }
                });
            setTag(res.data.tag);
        }
        getTag();
    }, [id]);


    return (
        <>
            <div style={{textAlign: "left", display: "flex", justifyContent: "space-between"}}>
                <h2>{tag?.name}</h2>
                <h2>{tag?.popularity}</h2>
            </div>
        </>
    )
}

export default Tag;