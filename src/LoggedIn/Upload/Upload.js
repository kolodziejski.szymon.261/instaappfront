import "./Upload.css";
import "../../variables.css";
import axios from "axios";
import {useCookies} from "react-cookie";
import Header from "../../header/Header";
import {useNavigate} from "react-router-dom";
import {useEffect} from "react";


function Upload() {
    const [cookies, setCookie, removeCookie] = useCookies();
    const navigate = useNavigate();
    useEffect(() => {
        const token = cookies.token;
        console.log(token);
        if (!token) navigate('/login');
    }, []);
    const upload = async () => {
        const fd = new FormData();
        fd.append('file', document.getElementById('ok').files[0]);
        fd.append('album', document.getElementById('album').value);
        fd.append('status', document.getElementById('status').value);
        try {
            await axios
                .post('http://localhost:3000/api/images', fd, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': `Bearer ${cookies.token}`,
                    }
                });
        } catch (e) {
            alert('Nie udało się dodać zdjęcia');
            return;
        }
        navigate('/');
    };


    return (
        <>
            <Header></Header>
            <main>
                <div className="test"></div>
                <div style={{marginTop: '3.5rem'}} className='uploadForm FormContainer'>
                    <input type="file" name="ok" id="ok"></input>
                    <input type="text" placeholder="album" name="album" id="album"/>
                    <select name="status" id="status">
                        <option value="private">Prywatny</option>
                        <option value="public">Publiczny</option>
                    </select>
                    <div className="spacer"></div>
                    <input type="submit" id='submit' className='button' onClick={upload} value="submit"/>
                </div>
            </main>
        </>
    )
}

export default Upload;