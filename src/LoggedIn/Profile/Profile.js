import "./Profile.css";
import "../../variables.css";
import {useNavigate, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {useCookies} from "react-cookie";
import Header from "../../header/Header";
import axios from "axios";

const Profile = () => {
    const navigate = useNavigate();
    const [cookies, setCookie, removeCookie] = useCookies();
    const [profile, setProfile] = useState({});
    const [trol, setTrol] = useState(Date.now());
    useEffect(() => {
        const token = cookies.token;
        console.log(token);
        if (!token) navigate('/login');

        getProfile();
    }, []);
    const getProfile = async () => {
        const res = await axios.get('http://localhost:3000/api/profile', {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${cookies.token}`,
            }
        });
        setProfile(res.data.profile);
    }


    const updateProfile = async () => {
        const user = {}
        let valid = true;
        const inputs = document.querySelectorAll('input');
        for (let input of inputs) {
            if (input.type === 'submit' || input.value === '') continue;
            if (input.value.length < 3) {
                setWrong(input)
                valid = false;
            }
            if (input.name === 'email' && !input.value.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)) {
                setWrong(input);
                valid = false;
            }
            user[input.name] = input.value === '' ? null : input.value;
        }
        if (!valid) return;

        const res = await fetch('http://localhost:3000/api/profile', {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${cookies.token}`,
            },
            body: JSON.stringify(user),
        });
        const data = await res.json();
        if (data.status === 400) {
            alert(data.message);
        }
        if (data.status === 200) {
            getProfile();
        }

    }

    const setWrong = (i) => {
        i.style.outline = '1px solid red';
        setTimeout(() => {
            i.style.outline = 'none';
        }, 500);
    }

    const updateProfilePicture = async (e) => {
        if (e.target.value === 'Delete') {
            try {
                await axios
                    .delete('http://localhost:3000/api/profile/picture', {
                        headers: {
                            'Authorization': `Bearer ${cookies.token}`,
                        }
                    });
            } catch (e) {
                alert('Nie udało się usunąć zdjęcia');
                return;
            }
            setTrol(Date.now());
            return;
        }
        if (!document.getElementById('ok').files[0]) return;
        const fd = new FormData();
        fd.append('file', document.getElementById('ok').files[0]);
        try {
            await axios
                .post('http://localhost:3000/api/profile/picture', fd, {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        'Authorization': `Bearer ${cookies.token}`,
                    }
                });
        } catch (e) {
            alert('Nie udało się zmienić zdjęcia');
            return;
        }
        console.log('ok')
        setTrol(Date.now());
    };

    return (
        <>
            <Header></Header>
            <main>
                <div className="FormContainer uploadForm" style={{marginTop: "2.5rem"}}>
                    <input type="file" name="ok" id="ok"></input>
                    <div className="spacer"></div>
                    <input onClick={updateProfilePicture} type={'submit'} name="submit" value="Delete"
                           id='submit'></input>
                    <input onClick={updateProfilePicture} type={'submit'} name="submit" value='Update'
                           id='submit'></input>
                </div>
                <div className="FormContainer" style={{marginTop: "2.5rem"}}>
                    <h2>
                        mail: {profile.email}
                    </h2>
                    <h3>
                        imie: {profile.imie}
                    </h3>
                    <h3>
                        nazwisko: {profile.nazwisko}
                    </h3>
                    <div className="spacer"></div>
                    <div id="wrapper">
                        <img id="pfp" src={`http://localhost:3000/api/profile/${profile._id}/?${trol}`} alt="niewiem"/>
                    </div>
                </div>
                <div className="FormContainer" style={{marginTop: "2.5rem"}}>
                    <input type="text" name="email" placeholder="email"/>
                    <input type="text" name="imie" placeholder="imię"/>
                    <input type="text" name="nazwisko" placeholder="nazwisko"/>
                    <input type="password" name="password" placeholder="hasło"/>
                    <div className="spacer"></div>
                    <input onClick={updateProfile} type={'submit'} name="submit" value={'Update'} id='submit'></input>
                </div>
            </main>
        </>
    )
}

export default Profile;